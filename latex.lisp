; Written by Dominik Derigs, University of Cologne
; Make maxima's tex() output AMSTeX like
; Party based on code written by R. J. Fateman, W. Shelter, C. Sangwin

;; LaTeX style quotients
(defun tex-mquotient (x l r)
  (if (or (null (cddr x)) (cdddr x)) (wna-err (caar x)))
  (setq l (tex (cadr x) (append l '("\\frac{")) nil 'mparen 'mparen)
	r (tex (caddr x) (list "}{") (append '("}") r) 'mparen 'mparen))
  (append l r))

;; amsmath style matrices
(defun tex-matrix (x l r)
  (append l `("\\begin{pmatrix} ")
	 (mapcan #'(lambda(y)
			  (tex-list (cdr y) nil (list " \\\\ ") " & "))
		 (cdr x))
	 '("\\end{pmatrix}") r))

;; make functions like sin(x) always look sin(x), and not the default sin x
(defun tex-prefix (x l r)
  (tex (cadr x) (append l (texsym (caar x)) '("\\left( ") )  (append '(" \\right)") r) 'mparen 'mparen))

;; Solve the problem that this would also display -1 as -(1)
(defprop mminus tex-prefix-unaryminus tex)
(defprop mminus ("-") texsym)
(defun tex-prefix-unaryminus (x l r)
  (tex (cadr x) (append l (texsym (caar x))) r (caar x) rop))

;; Change the display of derivatives
(defprop %derivative tex-derivative tex)
(defun tex-derivative (x l r)
  (tex (if $derivabbrev
	   (tex-dabbrev x)
	   (tex-d x '"\\mathrm{d}")) l r lop rop ))

;; Redefine some symbols
(mapc #'tex-setup
    '(
	(%acos "\\cos^{-1}")
	(%asin "\\sin^{-1}")
	(%atan "\\tan^{-1}")
))

;; Output LaTeX equations inside of
;; \begin{equation} xx \end{equation}
;; instead of the deprecated form
;; $$ xxx $$
(setq *tex-environment-default* `(,"\\begin{equation}
" . ,"
\\end{equation}"))
