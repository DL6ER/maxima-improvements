# README #

Make [maxima](http://maxima.sourceforge.net/)'s TeX output LaTeX (AMSTeX) compatible.

## Usage ##
Put the file(s) in your current working folder and type
```
loadfile("latex.lisp")$
```

### Quotients ###
```
tex1(1/a);
```
standard:
```
#!latex
{{1}\over{a}}
```
improved:
```
#!latex
\frac{1}{a}
```

### Matrices ###
```
tex1(matrix([a,b],[c,d]));
```
standard:
```
#!latex
\pmatrix{a&b\cr c&d\cr }
```
improved:
```
#!latex
\begin{pmatrix} a & b \\ c & d \\ \end{pmatrix}
```

### Functions ###
```
tex1(acos(a));
```
standard:

![latex_419523c53904aa1a68417897f5d4c611.png](https://bitbucket.org/repo/9ko7oX/images/4029630675-latex_419523c53904aa1a68417897f5d4c611.png)
```
#!latex
\arccos a
```

improved:

![latex_f78b206eca668335e8c11a8ac6ef09d7.png](https://bitbucket.org/repo/9ko7oX/images/634596206-latex_f78b206eca668335e8c11a8ac6ef09d7.png)
```
#!latex
\cos^{-1}\left( a \right)
```

## Equations ##
```
tex(a);
```
standard:
```
#!latex
$$a$$
```

improved:
```
#!latex
\begin{equation}
a
\end{equation}
```